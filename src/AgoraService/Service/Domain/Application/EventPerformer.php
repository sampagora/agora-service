<?php

namespace AgoraService\Service\Domain\Application;

use AgoraService\Service\Exception;

class EventPerformer extends \AgoraService\Service\AbstractService
{
     /**
     * Recupera os performers de um event
     * @param $eventId Int Id do perforner
     *
     * @return array
     */
    public function fetchAllByEventId($eventId)
    {
        $result = [];
        $eventPerformer = $this->getServiceLocator()
                              ->get('AgoraService\Dao\Mapper\Application\EventPerformer')
                              ->fetchList(['event_id' => $eventId]);
        if(count($eventPerformer)){
            foreach ($eventPerformer as $obj){
                $result[] = $this->getServiceLocator()
                                 ->get('AgoraService\Service\Domain\Application\Performer')
                                 ->fetchComplete($obj['performer_id']);
            }
        }
        
        return $result;
    }
}
