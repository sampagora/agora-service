<?php

namespace AgoraService\Service\Domain\Application;

use AgoraService\Service\Exception;

class PlaceHomepage extends \AgoraService\Service\AbstractService
{
     /**
     * Recupera as homepagrd de um performer
     * @param $placeId Int Id do perforner
     *
     * @return array
     */
    public function fetchAllCompleteByPlaceId($placeId)
    {
        $result = [];
        $placeHomepage = $this->getServiceLocator()
                                  ->get('AgoraService\Dao\Mapper\Application\PlaceHomepage')
                                  ->fetchList(['place_id' => $placeId]);
        if(count($placeHomepage)){
            foreach ($placeHomepage as $obj){
                $homepage = $this->getServiceLocator()
                                 ->get('AgoraService\Service\Domain\Application\Homepage')
                                 ->fetchOne($obj['homepage_id']);
                $result[$homepage['name']] = $obj['url'];
            }
        }
        
        return $result;
    }
}
