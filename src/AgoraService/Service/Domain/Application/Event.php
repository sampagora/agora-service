<?php

namespace AgoraService\Service\Domain\Application;

use AgoraService\Service\Exception;

class Event extends \AgoraService\Service\AbstractService
{
    /**
     * Recupera a lista de eventos
     *
     * @return array
     */
    public function fetchAllByDate($originalDate = null)
    {
        $result = [];
        if($originalDate){
            $date = \DateTime::createFromFormat('d/m/Y', $originalDate);
            $startDate = $date->format('Y-m-d 00:00:01');
            $tomorow = $date->modify('+1 day');
            $endDate = $tomorow->format('Y-m-d 23:59:59');
        }else{
            $startDate = date("Y-m-d 00:00:01");  
            $date = new \DateTime('+1 day');
            $endDate = $date->format('Y-m-d 23:59:59');
        }
        
        $events = $this->getServiceLocator()
                       ->get('AgoraService\Dao\Repository\Event')
                       ->fetchAllByDate($startDate, $endDate);
        if($events){
            foreach ($events as $i => $event){
                $result[$i]['id'] = $event['id'];
                $result[$i]['name'] = $event['name'];
                $result[$i]['url'] = $event['url'];
                $result[$i]['start'] = $event['start'];
                $result[$i]['end'] = $event['end'];
                $result[$i]['featured'] = $event['featured'];
                if($event['place_id']){
                    $result[$i]['place'] = $this->getServiceLocator()
                                                ->get('AgoraService\Service\Domain\Application\Place')
                                                ->fetchComplete($event['place_id']);
                }
                $result[$i]['zone'] = $this->getServiceLocator()
                                           ->get('AgoraService\Service\Domain\Application\Zone')
                                           ->fetchOne($event['zone_id']);
                $result[$i]['address']['name'] = $event['address'];
                $result[$i]['address']['number'] = $event['number'];
                $result[$i]['address']['district'] = $event['district'];
                $categories = $this->getServiceLocator()
                                   ->get('AgoraService\Service\Domain\Application\EventCategory')
                                   ->fetchAllByEventId($event['id']);
                if(count($categories)){
                    $result[$i]['event_categories'] = $categories;
                }
                
                $performers = $this->getServiceLocator()
                                   ->get('AgoraService\Service\Domain\Application\EventPerformer')
                                   ->fetchAllByEventId($event['id']);
                if(count($performers)){
                    $result[$i]['event_performers'] = $performers;
                }
            }
            
        }
        
        return $result;
    }
}
