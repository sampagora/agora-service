<?php

namespace AgoraService\Service\Domain\Application;

use AgoraService\Service\Exception;

class PerformerCategory extends \AgoraService\Service\AbstractService
{
     /**
     * Recupera as categorias de um performer
     * @param $performerId Int Id do perforner
     *
     * @return array
     */
    public function fetchAllCompleteByPerformerId($performerId)
    {
        $result = [];
        $performerCategory = $this->getServiceLocator()
                                  ->get('AgoraService\Dao\Mapper\Application\PerformerCategory')
                                  ->fetchList(['performer_id' => $performerId]);
        if(count($performerCategory)){
            foreach ($performerCategory as $obj){
                $category = $this->getServiceLocator()
                                  ->get('AgoraService\Service\Domain\Application\Category')
                                  ->fetchOne($obj['category_id']);
                $result[] = $category['name'];
            }
        }
        
        return $result;
    }
}
