<?php

namespace AgoraService\Service\Domain\Application;

use AgoraService\Service\Exception;

class PerformerHomepage extends \AgoraService\Service\AbstractService
{
     /**
     * Recupera as homepage de um performer
     * @param $performerId Int Id do perforner
     *
     * @return array
     */
    public function fetchAllCompleteByPerformerId($performerId)
    {
        $result = [];
        $homepage = $this->getServiceLocator()
                         ->get('AgoraService\Dao\Mapper\Application\PerformerHomepage')
                         ->fetchList(['performer_id' => $performerId]);
        if(count($homepage)){
            foreach ($homepage as $obj){
                $homepage = $this->getServiceLocator()
                                 ->get('AgoraService\Service\Domain\Application\Homepage')
                                 ->fetchOne($obj['homepage_id']);
                $result[$homepage['name']] = $obj['url'];
            }
        }
        
        return $result;
    }
}
