<?php

namespace AgoraService\Service\Domain\Application;

use AgoraService\Service\Exception;

class Performer extends \AgoraService\Service\AbstractService
{
    /**
     * Recupera o performer pelo id
     * @param $id Int Id do performer
     *
     * @return array
     */
    public function fetchOne($id)
    {
        return $this->getServiceLocator()
                    ->get('AgoraService\Dao\Mapper\Application\Performer')
                    ->fetch($id);
    }
    
    /**
     * Recupera o performer pelo id
     * @param $id Int Id do performer
     *
     * @return array
     */
    public function fetchComplete($id)
    {
        $result = [];
        $one = $this->fetchOne($id);
        if($one){
            $result = $one;
            $categories = $this->getServiceLocator()
                               ->get('AgoraService\Service\Domain\Application\PerformerCategory')
                               ->fetchAllCompleteByPerformerId($id);
            if(count($categories)){
                $result['categories'] = $categories;
            }
            $homepages = $this->getServiceLocator()
                              ->get('AgoraService\Service\Domain\Application\PerformerHomepage')
                              ->fetchAllCompleteByPerformerId($id);
            if(count($homepages)){
                $result['homepages'] = $homepages;
            }
        }
        
        return $result;
    }
}
