<?php

namespace AgoraService\Service\Domain\Application;

use AgoraService\Service\Exception;

class EventCategory extends \AgoraService\Service\AbstractService
{
     /**
     * Recupera as categorias de um event
     * @param $eventId Int Id do perforner
     *
     * @return array
     */
    public function fetchAllByEventId($eventId)
    {
        $result = [];
        $eventCategory = $this->getServiceLocator()
                              ->get('AgoraService\Dao\Mapper\Application\EventCategory')
                              ->fetchList(['event_id' => $eventId]);
        if(count($eventCategory)){
            foreach ($eventCategory as $obj){
                $category = $this->getServiceLocator()
                                 ->get('AgoraService\Service\Domain\Application\Category')
                                 ->fetchOne($obj['category_id']);
                $result[] = $category['name'];
            }
        }
        
        return $result;
    }
}
