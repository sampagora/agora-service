<?php

namespace AgoraService\Service\Domain\Application;

use AgoraService\Service\Exception;

class Category extends \AgoraService\Service\AbstractService
{
    /**
     * Recupera todas as Categorys
     *
     * @return array
     */
    public function fetchAll()
    {
        return $this->getServiceLocator()
                    ->get('AgoraService\Dao\Mapper\Application\Category')
                    ->fetchList();
    }
    
    /**
     * Recupera a categoria pelo id
     * @param $id Int Id da categoria
     *
     * @return array
     */
    public function fetchOne($id)
    {
        return $this->getServiceLocator()
                    ->get('AgoraService\Dao\Mapper\Application\Category')
                    ->fetch($id);
    }
}
