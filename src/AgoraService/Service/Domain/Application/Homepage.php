<?php

namespace AgoraService\Service\Domain\Application;

use AgoraService\Service\Exception;

class Homepage extends \AgoraService\Service\AbstractService
{
    /**
     * Recupera todas as Homepages
     *
     * @return array
     */
    public function fetchAll()
    {
        return $this->getServiceLocator()
                    ->get('AgoraService\Dao\Mapper\Application\Homepage')
                    ->fetchList();
    }
    
    /**
     * Recupera a categoria pelo id
     * @param $id Int Id da homepage
     *
     * @return array
     */
    public function fetchOne($id)
    {
        return $this->getServiceLocator()
                    ->get('AgoraService\Dao\Mapper\Application\Homepage')
                    ->fetch($id);
    }
}
