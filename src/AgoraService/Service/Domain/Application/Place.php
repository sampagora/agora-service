<?php

namespace AgoraService\Service\Domain\Application;

use AgoraService\Service\Exception;

class Place extends \AgoraService\Service\AbstractService
{
    /**
     * Recupera todas as Places
     *
     * @return array
     */
    public function fetchAll()
    {
        return $this->getServiceLocator()
                    ->get('AgoraService\Dao\Mapper\Application\Place')
                    ->fetchList();
    }
    
    /**
     * Recupera a categoria pelo id
     * @param $id Int Id da categoria
     *
     * @return array
     */
    public function fetchOne($id)
    {
        return $this->getServiceLocator()
                    ->get('AgoraService\Dao\Mapper\Application\Place')
                    ->fetch($id);
    }
    
    /**
     * Recupera o obejto compelto de place pelo id
     * @param $id Int Id do performer
     *
     * @return array
     */
    public function fetchComplete($id)
    {
        $result = [];
        $one = $this->fetchOne($id);
        if($one){
            $result = $one;
            if($one['zone_id']){
                $result['zone'] = $this->getServiceLocator()
                                        ->get('AgoraService\Service\Domain\Application\Zone')
                                        ->fetchOne($one['zone_id']);
            } 
            
            $placeHomepages = $this->getServiceLocator()
                                   ->get('AgoraService\Service\Domain\Application\PlaceHomepage')
                                   ->fetchAllCompleteByPlaceId($id);
            if(count($placeHomepages)){
                $result['homepages'] = $placeHomepages;
            }
        }
        
        return $result;
    }
}
