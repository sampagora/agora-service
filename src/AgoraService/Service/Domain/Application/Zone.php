<?php

namespace AgoraService\Service\Domain\Application;

use AgoraService\Service\Exception;

class Zone extends \AgoraService\Service\AbstractService
{
    /**
     * Recupera todas as Zones
     *
     * @return array
     */
    public function fetchAll()
    {
        return $this->getServiceLocator()
                    ->get('AgoraService\Dao\Mapper\Application\Zone')
                    ->fetchList();
    }
    
    /**
     * Recupera a zona pelo id
     * @param $id Int Id da zona
     *
     * @return array
     */
    public function fetchOne($id)
    {
        return $this->getServiceLocator()
                    ->get('AgoraService\Dao\Mapper\Application\Zone')
                    ->fetch($id);
    }
}
