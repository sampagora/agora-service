<?php
namespace AgoraService\Service\Entity;

interface InMemoryDataInterface
{

    public function fetch($id);
    public function fetchAll(array $filter = []);

}
