<?php
namespace AgoraService\Service\Entity\Application;

use AgoraService\Service\Entity\InMemoryDataAbstract;

class FaseConcurso extends InMemoryDataAbstract
{
    const BASE_INSCRICAO = 1;
    const BASE_HOMOLOGACAO = 2;
    const RES_PRELIMINAR_POME  = 3;
    const RES_FINAL_POME = 4;
    const RES_FINAL = 5;


    protected $data = [
        ['id' => 1, 'description' => 'Base Inscrição'],
        ['id' => 2, 'description' => 'Base Homologação'],
        ['id' => 3, 'description' => 'Resultado Preliminar POME'],
        ['id' => 4, 'description' => 'Resultado Final POME'],
        ['id' => 5, 'description' => 'Resultado Final']
    ];
    
}
