<?php
namespace AgoraService\Service\Entity;

use AgoraService\Service\AbstractService;
use AgoraService\Service\Exception;

abstract class InMemoryDataAbstract extends AbstractService implements InMemoryDataInterface
{

    public function fetch($id)
    {
        return $this->getOptionById($id, $this->data);
    }

    public function fetchOne($id)
    {
        return $this->getOptionById($id, $this->data);
    }

    public function fetchAll(array $filter = [])
    {
        if (! empty($filter)) {
            $filtered = [];

            $filterKeys = array_keys($filter);
            foreach ($filterKeys as $key) {
                foreach ($this->data as $data) {
                    if (isset($data[$key]) && in_array($data[$key], $filter[$key])) {
                        $filtered[] = $data;
                    }
                }
            }

            return $filtered;
        }

        return $this->data;
    }

    protected function getOptionById($id, array $options)
    {
        if (! $this->hasOptionById($id, $options)) {
            throw new Exception("ID não encontrado: $id");
        }

        $arrayKey = $this->getOptionArrayKeyById($id, $options);

        return $options[$arrayKey];
    }

    protected function hasOptionById($id, array $options)
    {
        if (! empty($options)) foreach ($options as $arrayKey => $option) {
            foreach ($option as $key => $value) {
                if ('id' == $key && $id == $value) {
                    return true;
                }
            }
        }

        return false;
    }

    protected function getOptionArrayKeyById($id, array $options)
    {
        if (! empty($options)) foreach ($options as $arrayKey => $option) {
            foreach ($option as $key => $value) {
                if ('id' == $key && $id == $value) {
                    return $arrayKey;
                }
            }
        }

        return [];
    }

}
