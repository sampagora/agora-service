<?php

namespace AgoraService\Service\Transaction;

class AbstractTransaction extends \AgoraService\Service\AbstractService
{
    protected function beginTransaction()
    {
        $this->getServiceLocator()->get('db.connection')->beginTransaction();
    }

    protected function rollBack()
    {
        $this->getServiceLocator()->get('db.connection')->rollback();
    }

    protected function commit()
    {
        $this->getServiceLocator()->get('db.connection')->commit();
    }
}
