<?php
namespace AgoraService\Dao\Mapper;

use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Sql\Expression as DbExpression;
use AgoraService\Dao\Exception\DuplicateKeyException;
use AgoraService\Dao\Exception\MapperException;

class DaoMapper
{
    protected $table;

    public function __construct(TableGatewayInterface $table)
    {
        $this->table = $table;
    }

    public function getTable()
    {
        return $this->table;
    }

    /**
     * Obtém o id do último registro inserido
     *
     * @param $pk String Chave primária da tabela
     * @return Int
     */
    public function getLastId($pk = 'id')
    {
        $select = $this->table->getSql()
                              ->select()
                              ->columns([
                                  'last_id' => new DbExpression("MAX({$pk})"),
                              ]);

        $results = $this->table->selectWith($select);

        return (int) $results->current()->offsetGet('last_id');
    }

    /**
     * Obtém o total de registros da tabela
     *
     * @param $pk String Chave primária da tabela
     * @return Int
     */
    public function getRecordsCount($pk = 'id')
    {
        $select = $this->table->getSql()
                              ->select()
                              ->columns([
                                  'count' => new DbExpression("COUNT({$pk})"),
                              ]);

        $results = $this->table->selectWith($select);

        return (int) $results->current()->offsetGet('count');
    }

    public function create($entity)
    {
        try {
            $this->table->insert($entity);
        } catch (\Exception $e) {
            $this->throwException($e);
        }

        return $this->table->getLastInsertValue();
    }

    public function update($id, $entity, $key = 'id')
    {
        try {
            $this->table->update($entity, [$key => $id]);
        } catch (\Exception $e) {
            $this->throwException($e);
        }
    }

    public function fetch($id, $key = 'id')
    {
        try {
            $row = $this->table->select([$key => $id]);
            $current = $row->current();

            if (! $current) {
                return [];
            }

            return $current->getArrayCopy();
        } catch (\Exception $e) {
            $this->throwException($e);
        }
    }

    public function fetchList($params = [], $order = null)
    {
        try {
            return $this->table->select(function ($select) use ($params, $order) {
                $select->where($params);

                if (! empty($order)) {
                    $select->order((array) $order);
                }
            })->toArray();
        } catch (\Exception $e) {
            $this->throwException($e);
        }
    }

    public function delete($id, $key = 'id')
    {
        try {
            $this->table->delete([$key => $id]);
        } catch (\Exception $e) {
            $this->throwException($e);
        }
    }

    public function deleteCollection($params = [])
    {
        try {
            $this->table->delete($params);
        } catch (\Exception $e) {
            $this->throwException($e);
        }
    }

    protected function throwException(\Exception $e)
    {
        if ($e instanceof \Zend\Db\Adapter\Exception\InvalidQueryException && preg_match('/23000\s-\s1062/', $e->getMessage())) {
            throw new DuplicateKeyException($e->getMessage(), $e->getCode(), $e);
        }
        throw new MapperException($e->getMessage(), $e->getCode(), $e);
    }
}
