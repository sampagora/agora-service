<?php

namespace AgoraService\Dao\Exception;

class DuplicateKeyException extends MapperException
{
}
