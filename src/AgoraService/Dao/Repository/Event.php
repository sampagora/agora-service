<?php

namespace AgoraService\Dao\Repository;

class Event extends \AgoraService\Service\AbstractService
{
    public function fetchAllByDate($startDate, $endDate)
    {
        $adapter = $this->getServiceLocator()->get('db.adapter');
        
        $sql = "SELECT  e.*
                FROM agoradb.event e
                LEFT JOIN agoradb.place p ON (p.id = e.place_id)
                LEFT JOIN agoradb.zone z ON (z.id = p.zone_id)
                WHERE (e.start > '$startDate' and e.end < '$endDate')
                and e.end > now()
                order by e.featured desc, e.start";
        return $adapter->query($sql, [])->toArray();
    }
}
