<?php
namespace AgoraService\AbstractFactory;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\AbstractFactoryInterface;

class DaoMapper implements AbstractFactoryInterface
{
    protected $schemaMap = [
        'Application'    => 'agoradb'
    ];

    protected function getMatches($requestedName)
    {
        $pattern = '@AgoraService\\\Dao\\\Mapper\\\(\w+)\\\(\w+)@i';

        if (! preg_match($pattern, trim($requestedName), $matches)) {
            return false;
        }

        if (! isset($this->schemaMap[$matches[1]])) {
            return false;
        }

        return $matches;
    }

    public function canCreateServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        $matches = $this->getMatches($requestedName);

        return (bool) $matches;
    }

    public function createServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        $matches = $this->getMatches($requestedName);

        // Schema no banco de dados deve ser o penúltimo sub-namespace
        $schemaName = $this->schemaMap[$matches[1]];
        // Tabela no banco de dados deve ser o último sub-namespace
        $tableName = (new \Zend\Filter\FilterChain(
                [
                    'filters' => [
                        [
                            'name'     => 'wordcamelcasetounderscore',
                            'priority' => 2,
                        ],
                        [
                            'name' => 'stringtolower',
                            'priority' => 1,
                        ],
                    ],
                ]
            ))
            ->filter($matches[2]);

        $tableIdentifier = new \Zend\Db\Sql\TableIdentifier($tableName, $schemaName);
        $table = new \Zend\Db\TableGateway\TableGateway($tableIdentifier, $serviceLocator->get('db.adapter'));

        return new \AgoraService\Dao\Mapper\DaoMapper($table);
    }
}
