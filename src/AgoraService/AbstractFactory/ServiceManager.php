<?php

namespace AgoraService\AbstractFactory;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\AbstractFactoryInterface;

class ServiceManager implements AbstractFactoryInterface
{
    public function canCreateServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        try {
            $rc = new \ReflectionClass($requestedName);
            $namespace = explode("\\", $rc->getNamespaceName());
            if (is_array($namespace) && $namespace[0] !== 'AgoraService') {
                return false;
            }
        } catch (\ReflectionException $e) {
            return false;
        }

        return $rc->implementsInterface('Zend\ServiceManager\ServiceLocatorAwareInterface');
    }

    public function createServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        $object = new $requestedName;
        $object->setServiceLocator($serviceLocator);

        return $object;
    }
}
