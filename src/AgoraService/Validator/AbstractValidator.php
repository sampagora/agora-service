<?php

namespace AgoraService\Validator;

abstract class AbstractValidator extends \AgoraService\Service\AbstractService
{
    protected $entity;

    public function initialize(array $entity)
    {
        $ao = $this->getServiceLocator()->get('AgoraService\ArrayObject');
        $ao->exchangeArray($entity);
        $this->entity = $ao;

        return $this;
    }
}
