<?php

namespace AgoraService\Validator;

class Exception extends \Exception
{
    protected $code = E_WARNING; 
}
