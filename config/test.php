<?php

return array(
   'db' => array(
      'driver' => 'Pdo_Mysql',
      'database' => 'agoradb',
      'username' => 'root',
      'password' => '',
      'hostname' => 'localhost',
      'port' => '3306',
      'driver_options' => array(1002 => 'SET NAMES \'UTF8\''),
   )
);