<?php

return array(
   'service_manager' => array(
      'abstract_factories' => array('AgoraService\AbstractFactory\ServiceManager', 'AgoraService\AbstractFactory\DaoMapper'),
      'factories' => array(
         'db.adapter' => function ($serviceManager) {
             $config = $serviceManager->get('config');
             $dbConfig = include __DIR__ . '/' . $config['agora-service-env'] . '.php';

             return new \Zend\Db\Adapter\Adapter($dbConfig['db']);
         },
         'db.connection' => function ($serviceManager) {
             return $serviceManager->get('db.adapter')->getDriver()->getConnection();
         }
                 
      ),
      'invokables' => array(
         'AgoraService\ArrayObject' => 'Zend\Stdlib\ArrayObject'
      ),
   )
);
