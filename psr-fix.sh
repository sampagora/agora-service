#!/bin/bash

php_cs_fixer='php-cs-fixer.phar'

if [ ! -e $php_cs_fixer ]; then
    echo -e "\nBaixando PHP CS Fixer\n"
    wget http://cs.sensiolabs.org/get/php-cs-fixer.phar
else
    echo "Atualizando PHP CS Fixer"
    php php-cs-fixer.phar self-update
fi

echo -e "\nExecutando PHP CS Fixer\n"
php php-cs-fixer.phar fix src/ --level=psr2
php php-cs-fixer.phar fix tests/Unit --level=psr2
php php-cs-fixer.phar fix tests/Functional --level=psr2
php php-cs-fixer.phar fix tests/Listener --level=psr2
