<?php

interface AgoraServiceListenerInterface
{
    public function loadDatabases();

    public function setFixtures();
}
