<?php

abstract class AgoraServiceListenerAbstract extends \PHPUnit_Framework_BaseTestListener implements AgoraServiceListenerInterface
{
    public function startTestSuite(PHPUnit_Framework_TestSuite $suite)
    {
        if ($suite->getName() === $this->suiteName) {
            $this->loadDatabases();
            $this->setFixtures();
        }
    }

    public function loadDatabases()
    {
        $config = \getDbConfig();

        foreach ($this->databaseFile as $dbFile) {
            $file = __DIR__ . '/../db-schemas/' . $dbFile;
            $cmd = "mysql -h {$config['hostname']} -u {$config['username']} -p{$config['password']} --port {$config['port']} < $file";
            $output = array();
            $return = null;
            exec($cmd, $output, $return);
            if ($return !== 0) {
                die("Não foi possível concluir testes: " . join("\n", $output) . "\n");
            }
        }
    }

    public function setFixtures()
    {
        $config = \getDbConfig();

        foreach ($this->fixtureFile as $fixFile) {
            $file = __DIR__ . '/../db-fixtures/' . $fixFile;
            $cmd = "mysql -h {$config['hostname']} -u {$config['username']} -p{$config['password']} --port {$config['port']} < $file";
            $output = array();
            $return = null;
            exec($cmd, $output, $return);
            if ($return !== 0) {
                die("Não foi possível carregar fixtures do teste: " . join("\n", $output) . "\n");
            }
        }
    }
}
