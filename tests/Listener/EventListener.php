<?php
class EventListener extends AgoraServiceListenerAbstract
{
    protected $suiteName = 'Event';
    protected $databaseFile = ['application.sql'];
    protected $fixtureFile = ['application.sql'];
}
