<?php
class ApplicationListener extends AgoraServiceListenerAbstract
{
    protected $suiteName = 'Application';
    protected $databaseFile = ['application.sql'];
    protected $fixtureFile = ['application.sql'];
}
