<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

require __DIR__ . '/../vendor/autoload.php';

function getServiceManager()
{
    $sm = new Zend\ServiceManager\ServiceManager();
    $sm->setService('config', array('agora-service-env' => 'test'));

    return $sm;
}

function getServiceManagerFromModule()
{
    $rootPath = realpath(dirname(__DIR__));

    $modules = array('AgoraService');
    $modulePaths = array(dirname($rootPath));

    $listenerOptions = new \Zend\ModuleManager\Listener\ListenerOptions(array('module_paths' => $modulePaths));
    $defaultListeners = new \Zend\ModuleManager\Listener\DefaultListenerAggregate($listenerOptions);
    $sharedEvents = new \Zend\EventManager\SharedEventManager();
    $moduleManager = new \Zend\ModuleManager\ModuleManager($modules);
    $moduleManager->getEventManager()->setSharedManager($sharedEvents);
    $moduleManager->getEventManager()->attachAggregate($defaultListeners);
    $moduleManager->loadModules();

    $config = $defaultListeners->getConfigListener()->getMergedConfig()->toArray();
    $smConfig = new Zend\Mvc\Service\ServiceManagerConfig($config['service_manager']);
    $serviceManager = new Zend\ServiceManager\ServiceManager($smConfig);
    $serviceManager->setService('config', array('agora-service-env' => 'test'));

    return $serviceManager;
}

function getDbConfig()
{
    $config = getServiceManagerFromModule()->get('config');
    $dbConfig = include __DIR__ . '/../config/' . $config['agora-service-env'] . '.php';

    return $dbConfig['db'];
}

function getFile($type = null, $limit = null)
{
    $type  = (! is_null($type)) ? $type : '*';
    $limit = (int) $limit;

    $matches = glob(__DIR__ . "/assets-fixtures/{$type}/*");
    shuffle($matches);

    if ($limit > 0) {
        $matches = array_slice($matches, 0, $limit);
    }

    $arr = [];
    foreach ($matches as $path) {
        $fileinfo = pathinfo($path);

        $filesize = function ($file, $decimals = 2) {
            $bytes = filesize($file);

            $sz = 'BKMGTP';
            $factor = floor((strlen($bytes) - 1) / 3);

            return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
        };

        $arr[] = [
            'tmp_name'  => $path,
            'name'      => $fileinfo['filename'].'.'.$fileinfo['extension'],
            'extension' => $fileinfo['extension'],
            'size'      => $filesize($path),
            'type'      => (new finfo)->file($path, FILEINFO_MIME_TYPE),
        ];
    }

    return (1 != $limit) ? $arr : array_shift($arr);
}
