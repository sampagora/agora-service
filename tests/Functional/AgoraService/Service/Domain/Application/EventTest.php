<?php

namespace Functional\AgoraService\Service\Domain\Application;

class EventTest extends \PHPUnit_Framework_TestCase
{
    
    public function testFetchAllByDateToday()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Domain\Application\Event');

        $actual = $service->fetchAllByDate();
        
        $place1 = [ 'id' => 1, 'zone_id' => 1, 'name' => 'Local 1',  'address' => 'Endereço 1', 'number' => '123', 'district' => 'Bairro 1',
                    'zone' => ['id' => 1, 'name' => 'Zona Norte'],
                    'homepages' =>  ['Facebook' => 'www.faceplace1.com']
                  ];
        
        $place2 = [ 'id' => 2, 'zone_id' => 2, 'name' => 'Local 2',  'address' => 'Endereço 2', 'number' => '321', 'district' => 'Bairro 2',
                    'zone' => ['id' => 2, 'name' => 'Zona Sul'],
                    'homepages' =>  [ 'Site' => 'www.siteplace1.com', 'Facebook' => 'www.faceplace2.com']
                  ];
        
        $performer1Complete = [
                                'id' => 1, 
                                'name' => 'Banda 1', 
                                'status' => 1, 
                                'categories' => ["Samba","Pagode"],
                                'homepages' =>  [ 'Site' => 'www.siteperformer1.com', 'Facebook' => 'www.faceperformer1.com'] 
                              ];
        
        $expected  = [
                        [
                            'id' => 2, 'name' => 'Evento 1.2', 'url' => 'www.site2.com', 'start' => '2018-01-08 23:59:00', 'end' => '2018-01-08 23:59:59', 'featured' => 1,
                            'place' => $place1,
                            'zone' => ['id' => 1, 'name' => 'Zona Norte'],
                            'address' => ['name' => 'Endereço 1', 'number' => '123', 'district' => 'Bairro 1'],
                            'event_categories' => ["Rock"],
                            'event_performers' =>   [
                                                        [
                                                            'id' => 2, 'name' => 'Banda 2', 'status' => 1, 
                                                            'homepages' =>  ['Facebook' => 'www.faceperformer2.com']
                                                        ],
                                                        [
                                                            'id' => 3, 'name' => 'Banda 3', 'status' => 1, 
                                                            'categories' => ["Rock"],
                                                        ]
                                                    ]
                        ],
                        [
                            'id' => 1, 'name' => 'Evento 1.1', 'url' => 'www.site.com', 'start' => '2018-01-08 00:01:00', 'end' => '2018-01-08 23:59:00', 'featured' => 0,
                            'place' => $place1,
                            'zone' => ['id' => 1, 'name' => 'Zona Norte'],
                            'address' => ['name' => 'Endereço 1', 'number' => '123', 'district' => 'Bairro 1'],
                            'event_categories' => ["Samba","Pagode"],
                            'event_performers' =>   [$performer1Complete]
                        ],
                        [
                            'id' => 5, 'name' => 'Evento 5', 'url' => 'www.site.com', 'start' => '2018-01-08 22:00:00', 'end' => '2018-01-08 23:00:00', 'featured' => 0,
                            'zone' => ['id' => 2, 'name' => 'Zona Sul'],
                            'address' => ['name' => 'Endereço 2', 'number' => '321', 'district' => 'Bairro 2'],
                            'event_categories' => ["Carnaval de Rua"],
                            'event_performers' =>   [$performer1Complete]
                        ],
                        [
                            'id' => 4, 'name' => 'Evento 1.1', 'url' => 'www.site.com', 'start' => '2018-01-08 23:59:00', 'end' => '2018-01-09 06:00:00', 'featured' => 0,
                            'place' => $place2,
                            'zone' => ['id' => 2, 'name' => 'Zona Sul'],
                            'address' => ['name' => 'Endereço 2', 'number' => '321', 'district' => 'Bairro 2'],
                            'event_categories' => ["Samba","Pagode"],
                            'event_performers' =>   [$performer1Complete]
                        ]
                       
                    ];

        $this->assertEquals($actual, $expected);
    }

    public function testFetchAllByDateTomorow()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Domain\Application\Event');

        $date = "03/01/2020";
        $actual = $service->fetchAllByDate($date);

        $place2 = [ 'id' => 2, 'zone_id' => 2, 'name' => 'Local 2',  'address' => 'Endereço 2', 'number' => '321', 'district' => 'Bairro 2',
                    'zone' => ['id' => 2, 'name' => 'Zona Sul'],
                    'homepages' =>  [ 'Site' => 'www.siteplace1.com', 'Facebook' => 'www.faceplace2.com']
                  ];
        
        $performer1Complete = [
                                'id' => 1, 
                                'name' => 'Banda 1', 
                                'status' => 1, 
                                'categories' => ["Samba","Pagode"],
                                'homepages' =>  [ 'Site' => 'www.siteperformer1.com', 'Facebook' => 'www.faceperformer1.com'] 
                              ];
        
        $expected  = [
                        [
                            'id' => 6, 'name' => 'Evento 6', 'url' => 'www.site5.com', 'start' => '2020-01-03 10:00:00', 'end' => '2020-01-03 15:00:00', 'featured' => 0,
                            'place' => $place2,
                            'zone' => ['id' => 2, 'name' => 'Zona Sul'],
                            'address' => ['name' => 'Endereço 2', 'number' => '321', 'district' => 'Bairro 2'],
                            'event_categories' => ["Samba","Pagode"],
                            'event_performers' =>   [$performer1Complete]
                        ]
                       
                    ];

        $this->assertEquals($actual, $expected);
    }
}
