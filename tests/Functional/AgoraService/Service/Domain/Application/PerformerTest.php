<?php

namespace Functional\AgoraService\Service\Domain\Application;

class PerformerTest extends \PHPUnit_Framework_TestCase
{
    public function testFetchOne()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Domain\Application\Performer');
        
        $actual = $service->fetchOne(2);
        $expected  = ['id' => 2, 'name' => 'Banda 2', 'status' => 1];

        $this->assertEquals($actual, $expected);
    }
    
    public function testFetchOneReturnNull()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Domain\Application\Zone');
        
        $actual = $service->fetchOne(6);
        $expected  = [];

        $this->assertEquals($actual, $expected);
    }
    
    public function testFetchComplete()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Domain\Application\Performer');
        
        $actual = $service->fetchComplete(1);
        $expected  = [
                        'id' => 1, 
                        'name' => 'Banda 1',
                        'status' => 1,
                        'categories' => ["Samba","Pagode"],
                        'homepages' =>  [ 'Site' => 'www.siteperformer1.com', 'Facebook' => 'www.faceperformer1.com']
                    ];

        $this->assertEquals($actual, $expected);
    }
    
    public function testFetchCompleteReturnNull()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Domain\Application\Performer');
        
        $actual = $service->fetchComplete(6);
        $expected  = [];

        $this->assertEquals($actual, $expected);
    }
    
    public function testFetchCompleteReturnCategoriesNull()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Domain\Application\Performer');
        
        $actual = $service->fetchComplete(5);
        $expected  = [
                        'id' => 5, 
                        'name' => 'Banda 5',
                        'status' => 0,
                        'homepages' =>  [ 'Site' => 'www.siteperformer5.com']
                    ];
        
        $this->assertEquals($actual, $expected);
    }
}

