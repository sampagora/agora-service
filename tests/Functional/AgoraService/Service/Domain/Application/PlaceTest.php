<?php

namespace Functional\AgoraService\Service\Domain\Application;

class PlaceTest extends \PHPUnit_Framework_TestCase
{
    
    public function testFetchAll()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Domain\Application\Place');
        
        $actual = $service->fetchAll();
        $expected  = [
                        ['id' => 1, 'zone_id' => 1, 'name' => 'Local 1',  'address' => 'Endereço 1', 'number' => '123', 'district' => 'Bairro 1'],
                        ['id' => 2, 'zone_id' => 2, 'name' => 'Local 2',  'address' => 'Endereço 2', 'number' => '321', 'district' => 'Bairro 2'],
                        ['id' => 3, 'zone_id' => null, 'name' => 'Local 3',  'address' => 'Endereço 3', 'number' => '321', 'district' => 'Bairro 3'],
                    ];

        $this->assertEquals($actual, $expected);
    }
    
    public function testFetchComplete()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Domain\Application\Place');
        
        $actual = $service->fetchComplete(2);
        $expected = [   'id' => 2, 'zone_id' => 2, 'name' => 'Local 2',  'address' => 'Endereço 2', 'number' => '321', 'district' => 'Bairro 2',
                        'zone' => ['id' => 2, 'name' => 'Zona Sul'],
                        'homepages' =>  [ 'Site' => 'www.siteplace1.com', 'Facebook' => 'www.faceplace2.com']
                    ];
        
        $this->assertEquals($actual, $expected);
    }
    
    public function testFetchCompleteOfZoneAndHomepagesIsEmpty()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Domain\Application\Place');
        
        $actual = $service->fetchComplete(3);
        $expected = ['id' => 3, 'zone_id' => null, 'name' => 'Local 3',  'address' => 'Endereço 3', 'number' => '321', 'district' => 'Bairro 3'];

        $this->assertEquals($actual, $expected);
    }
    
    public function testFetchCompleteReturnNull()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Domain\Application\Place');
        
        $actual = $service->fetchComplete(5);
        $expected  = [];

        $this->assertEquals($actual, $expected);
    }
}
