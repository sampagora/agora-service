<?php

namespace Functional\AgoraService\Service\Domain\Application;

class ZoneTest extends \PHPUnit_Framework_TestCase
{
    
    public function testFetchAll()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Domain\Application\Zone');
        
        $actual = $service->fetchAll();
        $expected  = [
                        ['id' => 1, 'name' => 'Zona Norte'],
                        ['id' => 2, 'name' => 'Zona Sul'],
                        ['id' => 3, 'name' => 'Zona Leste'],
                        ['id' => 4, 'name' => 'Zona Oeste'],
                    ];

        $this->assertEquals($actual, $expected);
    }
    
    public function testFetchOne()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Domain\Application\Zone');
        
        $actual = $service->fetchOne(1);
        $expected  = ['id' => 1, 'name' => 'Zona Norte'];

        $this->assertEquals($actual, $expected);
    }
    
    public function testFetchOneReturnNull()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Domain\Application\Zone');
        
        $actual = $service->fetchOne(5);
        $expected  = [];

        $this->assertEquals($actual, $expected);
    }
}
