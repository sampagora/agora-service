<?php

namespace Functional\AgoraService\Service\Domain\Application;

class HomepageTest extends \PHPUnit_Framework_TestCase
{
    
    public function testFetchAll()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Domain\Application\Homepage');
        
        $actual = $service->fetchAll();
        $expected  = [
                        ['id' => 1, 'name' => 'Site'],
                        ['id' => 2, 'name' => 'Facebook'],
                    ];

        $this->assertEquals($actual, $expected);
    }
    
    public function testFetchOne()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Domain\Application\Homepage');
        
        $actual = $service->fetchOne(1);
        $expected  = ['id' => 1, 'name' => 'Site'];

        $this->assertEquals($actual, $expected);
    }
    
    public function testFetchOneReturnNull()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Domain\Application\Homepage');
        
        $actual = $service->fetchOne(5);
        $expected  = [];

        $this->assertEquals($actual, $expected);
    }
}
