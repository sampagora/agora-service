<?php

namespace Functional\AgoraService\Service\Domain\Application;

class CategoryTest extends \PHPUnit_Framework_TestCase
{
    
    public function testFetchAll()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Domain\Application\Category');
        
        $actual = $service->fetchAll();
        $expected  = [
                        ['id' => 1, 'name' => 'Samba'],
                        ['id' => 2, 'name' => 'Pagode'],
                        ['id' => 3, 'name' => 'Rock'],
                        ['id' => 4, 'name' => 'Evento Esportivo'],
                        ['id' => 5, 'name' => 'Carnaval de Rua'],
                    ];

        $this->assertEquals($actual, $expected);
    }
    
    public function testFetchOne()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Domain\Application\Category');
        
        $actual = $service->fetchOne(1);
        $expected  = ['id' => 1, 'name' => 'Samba'];

        $this->assertEquals($actual, $expected);
    }
    
    public function testFetchOneReturnNull()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Domain\Application\Category');
        
        $actual = $service->fetchOne(6);
        $expected  = [];

        $this->assertEquals($actual, $expected);
    }
}
