<?php

namespace Functional\AgoraService\Service\Entity\Application;

class FaseConcursoTest extends \PHPUnit_Framework_TestCase
{
    
    public function testFetchIdFaseConcurso()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Entity\Application\FaseConcurso');
        
        $actual = $service->fetch(1);
        $expected = ['id' => 1, 'description' => 'Base Inscrição'];

        $this->assertEquals($actual, $expected);
    }
    
    public function testFetchAllFasesConcurso()
    {
        $sm = \getServiceManagerFromModule();
        $service = $sm->get('AgoraService\Service\Entity\Application\FaseConcurso');
        
        $actual = $service->fetchAll(array());
        
        $expected  = [
                        ['id' => 1, 'description' => 'Base Inscrição'],
                        ['id' => 2, 'description' => 'Base Homologação'],
                        ['id' => 3, 'description' => 'Resultado Preliminar POME'],
                        ['id' => 4, 'description' => 'Resultado Final POME'],
                        ['id' => 5, 'description' => 'Resultado Final']
                    ];

        $this->assertEquals($actual, $expected);
    }
}
