USE `agoradb`;

INSERT INTO zone VALUES (1, 'Zona Norte');
INSERT INTO zone VALUES (2, 'Zona Sul');
INSERT INTO zone VALUES (3, 'Zona Leste');
INSERT INTO zone VALUES (4, 'Zona Oeste');

INSERT INTO category VALUES (1, 'Samba');
INSERT INTO category VALUES (2, 'Pagode');
INSERT INTO category VALUES (3, 'Rock');
INSERT INTO category VALUES (4, 'Evento Esportivo');
INSERT INTO category VALUES (5, 'Carnaval de Rua');

INSERT INTO homepage VALUES (1, 'Site');
INSERT INTO homepage VALUES (2, 'Facebook');

INSERT INTO place VALUES (1, 1, 'Local 1',  'Endereço 1', '123', 'Bairro 1');
INSERT INTO place_homepage VALUES (1,1,2,'www.faceplace1.com');
INSERT INTO place VALUES (2, 2, 'Local 2',  'Endereço 2', '321', 'Bairro 2');
INSERT INTO place_homepage VALUES (2,2,1,'www.siteplace1.com');
INSERT INTO place_homepage VALUES (3,2,2,'www.faceplace2.com');
INSERT INTO place VALUES (3, null, 'Local 3', 'Endereço 3', '321', 'Bairro 3');

INSERT INTO performer VALUES (1,'Banda 1',1);
INSERT INTO performer_category VALUES (1,1,1);
INSERT INTO performer_category VALUES (2,1,2);
INSERT INTO performer_homepage VALUES (1,1,1,'www.siteperformer1.com');
INSERT INTO performer_homepage VALUES (2,1,2,'www.faceperformer1.com');

INSERT INTO performer VALUES (2, 'Banda 2',1);
INSERT INTO performer_homepage VALUES (3,2,2,'www.faceperformer2.com');
INSERT INTO performer VALUES (3, 'Banda 3',1);
INSERT INTO performer_category VALUES (3,3,3);
INSERT INTO performer VALUES (4, 'Palmeiras',1);
INSERT INTO performer_category VALUES (4,4,4);
INSERT INTO performer VALUES (5, 'Banda 5',0);
INSERT INTO performer_homepage VALUES (4,5,1,'www.siteperformer5.com');

INSERT INTO event VALUES (1, 1, 1,'Endereço 1', '123', 'Bairro 1',  'Evento 1.1', 'www.site.com', "2018-01-08 00:01:00", "2018-01-08 23:59:00", 0);
INSERT INTO event_performer VALUES (1, 1, 1);
INSERT INTO event_category VALUES (1, 1, 1);
INSERT INTO event_category VALUES (2, 1, 2);

INSERT INTO event VALUES (2, 1, 1,'Endereço 1', '123', 'Bairro 1',  'Evento 1.2', 'www.site2.com', "2018-01-08 23:59:00", "2018-01-08 23:59:59", 1);
INSERT INTO event_performer VALUES (2, 2, 2);
INSERT INTO event_performer VALUES (3, 2, 3);
INSERT INTO event_category VALUES (3, 2, 3);

INSERT INTO event VALUES (3, 1, 1,'Endereço 1', '123', 'Bairro 1',  'Evento 1.1', 'www.site.com', "2018-01-08 00:00:10", "2018-01-08 00:01:00", 1);

INSERT INTO event VALUES (4, 2, 2,'Endereço 2', '321', 'Bairro 2',  'Evento 1.1', 'www.site.com', "2018-01-08 23:59:00", "2018-01-09 06:00:00", 0);
INSERT INTO event_performer VALUES (4, 4, 1);
INSERT INTO event_category VALUES (4, 4, 1);
INSERT INTO event_category VALUES (5, 4, 2);

INSERT INTO event VALUES (5, null, 2,'Endereço 2', '321', 'Bairro 2',  'Evento 5', 'www.site.com', "2018-01-08 22:00:00", "2018-01-08 23:00:00", 0);
INSERT INTO event_performer VALUES (5, 5, 1);
INSERT INTO event_category VALUES (6, 5, 5);

########### De amanha ####################

INSERT INTO event VALUES (6, 2, 2, 'Endereço 2', '321', 'Bairro 2',  'Evento 6', 'www.site5.com', "2020-01-03 10:00:00", "2020-01-03 15:00:00", 0);
INSERT INTO event_performer VALUES (6, 6, 1);
INSERT INTO event_category VALUES (7, 6, 1);
INSERT INTO event_category VALUES (8, 6, 2);





