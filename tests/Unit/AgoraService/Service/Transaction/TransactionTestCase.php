<?php

namespace Unit\AgoraService\Service\Transaction;

abstract class TransactionTestCase extends \PHPUnit_Framework_TestCase
{
    protected function getBeginTransStub()
    {
        return $this->getConnectionStub('beginTransaction');
    }

    protected function getRollbackStub()
    {
        return $this->getConnectionStub('rollback');
    }

    protected function getCommitStub()
    {
        return $this->getConnectionStub('commit');
    }

    private function getConnectionStub($method)
    {
        $connectionStub = $this->getMockBuilder('Zend\Db\Adapter\Driver\Pdo\Connection')->disableOriginalConstructor()->getMock();
        $connectionStub->expects($this->once())->method($method);

        return $connectionStub;
    }
}
