-- MySQL Workbench Synchronization
-- Generated: 2017-08-18 12:48
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: michelpena

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP DATABASE agoradb;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`agoradb` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `agoradb`;

CREATE TABLE IF NOT EXISTS `event` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `place_id` INT(11) NULL DEFAULT NULL,
  `zone_id` INT(11) NULL DEFAULT NULL,
  `address` VARCHAR(255) NOT NULL,
  `number` VARCHAR(255) NOT NULL,
  `district` VARCHAR(255) NULL DEFAULT NULL,
  `name` VARCHAR(255) NOT NULL,
  `url` VARCHAR(255) NULL DEFAULT NULL,
  `start` DATETIME NOT NULL,
  `end` DATETIME NOT NULL,
  `featured` SMALLINT(6) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_event_place_idx` (`place_id` ASC),
  CONSTRAINT `fk_event_place` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_zone` FOREIGN KEY (`zone_id`) REFERENCES `zone` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `performer` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `status` TINYINT(1) NULL DEFAULT '1' ,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `category` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `place` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `zone_id` INT(11) NULL DEFAULT NULL,
  `name` VARCHAR(255) NOT NULL,
  `address` VARCHAR(255) NOT NULL,
  `number` VARCHAR(255) NOT NULL,
  `district` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_place_zone1_idx` (`zone_id` ASC),
  CONSTRAINT `fk_place_zone`
    FOREIGN KEY (`zone_id`)
    REFERENCES `zone` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `homepage` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COMMENT = '1- site\n2 - facebook\n3 - twiter';

CREATE TABLE IF NOT EXISTS `zone` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `event_category` (
  `id` INT(11) NOT NULL,
  `event_id` INT(11) NOT NULL,
  `category_id` INT(11) NOT NULL,
  INDEX `fk_event_category_category_idx` (`category_id` ASC),
  INDEX `fk_event_category_event_idx` (`event_id` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_event_category_event`
    FOREIGN KEY (`event_id`)
    REFERENCES `event` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_category_category`
    FOREIGN KEY (`category_id`)
    REFERENCES `category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `performer_category` (
  `id` INT(11) NOT NULL,
  `performer_id` INT(11) NOT NULL,
  `category_id` INT(11) NOT NULL,
  INDEX `fk_performer_category_category_idx` (`category_id` ASC),
  INDEX `fk_performer_category_performer_idx` (`performer_id` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_performer_category_performer`
    FOREIGN KEY (`performer_id`)
    REFERENCES `performer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_performer_category_category`
    FOREIGN KEY (`category_id`)
    REFERENCES `category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `event_performer` (
  `id` INT(11) NOT NULL,
  `event_id` INT(11) NOT NULL,
  `performer_id` INT(11) NOT NULL,
  INDEX `fk_event_performer_performer_idx` (`performer_id` ASC),
  INDEX `fk_event_performer_event_idx` (`event_id` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_event_performer_event`
    FOREIGN KEY (`event_id`)
    REFERENCES `event` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_performer_performer`
    FOREIGN KEY (`performer_id`)
    REFERENCES `performer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `place_homepage` (
  `id` INT(11) NOT NULL,
  `place_id` INT(11) NOT NULL,
  `homepage_id` INT(11) NOT NULL,
  `url` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_place_homepage_homepage_idx` (`homepage_id` ASC),
  INDEX `fk_place_homepage_place_idx` (`place_id` ASC),
  CONSTRAINT `fk_place_homepage_place`
    FOREIGN KEY (`place_id`)
    REFERENCES `place` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_place_homepage_homepage`
    FOREIGN KEY (`homepage_id`)
    REFERENCES `homepage` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `performer_homepage` (
  `id` INT(11) NOT NULL,
  `performer_id` INT(11) NOT NULL,
  `homepage_id` INT(11) NOT NULL,
  `url` VARCHAR(255) NOT NULL,
  INDEX `fk_performer_homepage_homepage_idx` (`homepage_id` ASC),
  INDEX `fk_performer_homepage_performer_idx` (`performer_id` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_performer_homepage_performer`
    FOREIGN KEY (`performer_id`)
    REFERENCES `performer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_performer_homepage_homepage`
    FOREIGN KEY (`homepage_id`)
    REFERENCES `homepage` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
